package cx.ath.rumil.android.kontodroid;

import android.app.Application;
import android.content.SharedPreferences;
import cx.ath.rumil.android.kontodroid.common.Consts;
import cx.ath.rumil.kontodroid.restclient.service.KontomierzClient;

import static android.text.TextUtils.isEmpty;

public class Kontodroid extends Application {

    private KontomierzClient client;

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences preferences = getSharedPreferences(Consts.Prefs.PREFS_NAME, MODE_PRIVATE);
        if (preferences.contains(Consts.Prefs.USERNAME) && preferences.contains(Consts.Prefs.PASSWORD)) {
            initClient(preferences.getString(Consts.Prefs.USERNAME, null), preferences.getString(Consts.Prefs.PASSWORD, null));
        }
    }

    public void initClient(String username, String password) {
        if (!isEmpty(username) && !isEmpty(password)) {
            client = new KontomierzClient("https://kontomierz.pl", username, password);
        } else {
            client = null;
        }
    }

    public KontomierzClient getClient() {
        return client;
    }
}

package cx.ath.rumil.android.kontodroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import cx.ath.rumil.android.kontodroid.R;
import cx.ath.rumil.kontodroid.restclient.common.CategoryGroup;

import java.util.List;

public class CategoryListAdapter extends BaseExpandableListAdapter {


    private LayoutInflater inflater;
    private List<CategoryGroup> categoryGroups;

    public CategoryListAdapter(Context context, List<CategoryGroup> categoryGroups) {
        this.categoryGroups = categoryGroups;
        inflater = LayoutInflater.from(context);
    }


    @Override
    //counts the number of group/parent items so the list knows how many times calls getGroupView() method
    public int getGroupCount() {
        return categoryGroups.size();
    }

    @Override
    //counts the number of children items so the list knows how many times calls getChildView() method
    public int getChildrenCount(int i) {
        return categoryGroups.get(i).getCategories().size();
    }

    @Override
    //gets the title of each parent/group
    public Object getGroup(int i) {
        return categoryGroups.get(i).getName();
    }

    @Override
    //gets the name of each item
    public Object getChild(int i, int i1) {
        return categoryGroups.get(i).getCategories().get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    //in this method you must set the text to see the parent/group on the list
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

        if (view == null) {
            view = inflater.inflate(R.layout.category_list_item_parent, viewGroup, false);
        }

        TextView textView = (TextView) view.findViewById(R.id.category_group_name);
        //"i" is the position of the parent/group in the list
        textView.setText(getGroup(i).toString());

        //return the entire view
        return view;
    }

    @Override
    //in this method you must set the text to see the children on the list
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.category_list_item_child, viewGroup, false);
        }

        TextView textView = (TextView) view.findViewById(R.id.category_name);
        //"i" is the position of the parent/group in the list and 
        //"i1" is the position of the child
        textView.setText(categoryGroups.get(i).getCategories().get(i1).getName());

        //return the entire view
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
package cx.ath.rumil.android.kontodroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentTransaction;
import android.widget.ExpandableListView;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import cx.ath.rumil.android.kontodroid.adapters.CategoryListAdapter;
import cx.ath.rumil.android.kontodroid.common.Consts;
import cx.ath.rumil.kontodroid.restclient.common.CategoryGroup;
import cx.ath.rumil.kontodroid.restclient.service.KontomierzClient;

import java.util.List;

import static com.actionbarsherlock.app.ActionBar.Tab;

public class HelloAndroidActivity extends SherlockActivity implements ActionBar.TabListener {

    private ExpandableListView mExpandableList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.main);
        /*getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        for (int i = 1; i <= 3; i++) {
            Tab tab = getSupportActionBar().newTab();
            tab.setText("Tab " + i);
            tab.setTabListener(this);
            getSupportActionBar().addTab(tab);
        }*/

        mExpandableList = (ExpandableListView) findViewById(R.id.expandable_list);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshPrefs();

        Kontodroid kontodroid = (Kontodroid) getApplication();

        KontomierzClient client = kontodroid.getClient();

        if (client != null) {
            List<CategoryGroup> categoryGroups = client.getCategories();
            mExpandableList.setAdapter(new CategoryListAdapter(this, categoryGroups));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (isUserLoggedIn()) {
            MenuItem item = menu.add("Log out");
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
            Intent loginIntent = new Intent();
            loginIntent.setClass(this, LoginActivity.class);
            item.setIntent(loginIntent);
        } else {
            MenuItem item = menu.add("Log in");
            item.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
        }

        return true;
    }

    private boolean isUserLoggedIn() {
        SharedPreferences prefs = getSharedPreferences(Consts.Prefs.PREFS_NAME, MODE_PRIVATE);
        return prefs.contains(Consts.Prefs.USERNAME);
    }

    private void refreshPrefs() {
        SharedPreferences prefs = getSharedPreferences(Consts.Prefs.PREFS_NAME, MODE_PRIVATE);
        if (!prefs.contains(Consts.Prefs.USERNAME)) {
            Intent loginIntent = new Intent();
            loginIntent.setClass(this, LoginActivity.class);

            startActivity(loginIntent);
            return;
        }

        String username = prefs.getString(Consts.Prefs.USERNAME, null);
        Toast.makeText(this, "Username from prefs is: " + username, 1000).show();
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction transaction) {
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction transaction) {
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction transaction) {
    }

}


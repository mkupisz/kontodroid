package cx.ath.rumil.android.kontodroid.common;

public final class Consts {

    public static final String TAG = "Kontodroid";

    public static final class Prefs {
        public static final String PREFS_NAME = "kontodroid";
        public static final String USERNAME = "user.username";
        public static final String PASSWORD = "user.password";
    }
}

package cx.ath.rumil.android.kontodroid;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.actionbarsherlock.app.SherlockActivity;
import cx.ath.rumil.android.kontodroid.common.Consts;

import static android.text.TextUtils.isEmpty;
import static cx.ath.rumil.android.kontodroid.common.Utils.makeToast;

public class LoginActivity extends SherlockActivity {

    private EditText mUsername;
    private EditText mPassword;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login);
        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);
    }

    /**
     * Called when the user clicks save button
     */
    public void saveUsername(View view) {
        SharedPreferences preferences = getSharedPreferences(Consts.Prefs.PREFS_NAME, MODE_PRIVATE);
        String username = mUsername.getText().toString();
        String password = mPassword.getText().toString();

        boolean result;
        if (isEmpty(username) && isEmpty(password)) {
            result = preferences.edit()
                    .remove(Consts.Prefs.USERNAME)
                    .remove(Consts.Prefs.PASSWORD)
                    .commit();
        } else {
            result = preferences.edit()
                    .putString(Consts.Prefs.USERNAME, username)
                    .putString(Consts.Prefs.PASSWORD, password)
                    .commit();
        }
        if (!result) {
            Log.e(Consts.TAG, "Saving username failed");
            makeToast(this, "Could not save username and password");
        } else {
            Kontodroid app = (Kontodroid) getApplication();
            app.initClient(username, password);
        }
    }
}
package cx.ath.rumil.kontodroid.restclient.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import cx.ath.rumil.kontodroid.restclient.common.CategoryGroup;
import cx.ath.rumil.kontodroid.restclient.common.CategoryGroups;
import org.springframework.http.*;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class KontomierzClient {

    private RestTemplate restTemplate;
    private HttpHeaders requestHeaders;
    private String url;

    public KontomierzClient(String url, String username, String password) {
        if(username == null || password == null)
        {
            throw new IllegalArgumentException("Username and password cannot be null");
        }
        restTemplate = new RestTemplate();
        GsonHttpMessageConverter gsonConverter = new GsonHttpMessageConverter();
        gsonConverter.setGson(new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create());
        restTemplate.getMessageConverters().add(gsonConverter);

        HttpAuthentication authHeader = new HttpBasicAuthentication(username, password);
        requestHeaders = new HttpHeaders();
        requestHeaders.setAuthorization(authHeader);

        this.url = url;
    }

    public List<CategoryGroup> getCategories() {
        HttpEntity<CategoryGroup> requestEntity = new HttpEntity<CategoryGroup>(requestHeaders);
        ResponseEntity<CategoryGroups> response = restTemplate.exchange(url + "/category_groups.json", HttpMethod.GET, requestEntity, CategoryGroups.class);
        return response.getBody().getCategoryGroups();
    }

}

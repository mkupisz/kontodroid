package cx.ath.rumil.kontodroid.restclient.common;

import java.util.List;

public class CategoryGroups {
    private List<CategoryGroup> categoryGroups;

    public List<CategoryGroup> getCategoryGroups() {
        return categoryGroups;
    }

    public void setCategoryGroups(List<CategoryGroup> categoryGroups) {
        this.categoryGroups = categoryGroups;
    }
}

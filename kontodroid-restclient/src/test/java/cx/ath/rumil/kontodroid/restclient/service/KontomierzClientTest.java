package cx.ath.rumil.kontodroid.restclient.service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.xtremelabs.robolectric.Robolectric;
import com.xtremelabs.robolectric.RobolectricTestRunner;
import cx.ath.rumil.kontodroid.restclient.common.CategoryGroup;
import cx.ath.rumil.kontodroid.restclient.common.CategoryGroups;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

@RunWith(RobolectricTestRunner.class)
public class KontomierzClientTest {

    private static final Header JSON_HEADER = new BasicHeader("Content-type", "application/json");

    private KontomierzClient client = new KontomierzClient("", "", "");

    @Test
    public void testDeserialization() throws Exception {
        String response = getResponseFromFile("category_groups.json");
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        CategoryGroups c = gson.fromJson(response, CategoryGroups.class);
        assertNotNull(c);
        assertNotNull(c.getCategoryGroups());
        assertFalse(c.getCategoryGroups().isEmpty());
        assertNotNull(c.getCategoryGroups().get(0));
        assertNotNull(c.getCategoryGroups().get(0).getCategories());
        assertNotNull(c.getCategoryGroups().get(0).getCategories().get(0));
        assertNotNull(c.getCategoryGroups().get(0).getCategories().get(0).getName());
    }

    @Test
    public void testLogin() throws Exception {

        String response = getResponseFromFile("category_groups.json");
        Robolectric.addPendingHttpResponse(200, response, JSON_HEADER);
        System.out.println(response);
        List<CategoryGroup> categories = client.getCategories();

        assertNotNull(categories);
    }

    private String getResponseFromFile(String filename) throws IOException {
        System.out.println(this.getClass().getResource(filename).getFile());
        FileInputStream stream = new FileInputStream(this.getClass().getResource(filename).getFile());
        try {
            FileChannel fc = stream.getChannel();
            MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
            /* Instead of using default, pass in a decoder. */
            return Charset.defaultCharset().decode(bb).toString();
        } finally {
            stream.close();
        }
    }
}

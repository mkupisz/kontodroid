package cx.ath.rumil.android.kontodroid.test;

import android.test.ActivityInstrumentationTestCase2;
import cx.ath.rumil.android.kontodroid.LoginActivity;

public class LoginActivityTest extends ActivityInstrumentationTestCase2<LoginActivity> {

    public LoginActivityTest() {
        super(LoginActivity.class);
    }

    public void testActivity() {
        LoginActivity activity = getActivity();
        assertNotNull(activity);
    }
}

